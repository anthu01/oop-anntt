<?php
    include_once('C:\laragon\www\PHP_OOP\dao\Database.php');
    include_once('C:\laragon\www\PHP_OOP\interface\IDao.php');

    abstract class BaseDao implements IDao
    {
        protected $database;

        function __construct()
        {
            $this->database = Database::getInstants();
        }

        /**
        * Get name table from param row
        * @param $row
        * @return string
        */
        public function getTableName($row)
        {
            return strtolower(get_class($row)).'Table';
        }

        /**
         * Insert Row to table
         * @param $row
         * @return boolean
         */
        public function insert( BaseRow $row)
        {
            if ($this->database->insertTable($this->getTableName($row), $row)){
                return true;
            }
            return false;

        }

        /**
         * Update Row to table
         * @param $row
         * @return mixed
         */
        public function update(BaseRow $row)
        {
            return $this->database->updateTable($this->getTableName($row), $row);
        }   
        
        /**
         * Delete Row from table
         * @param $row
         * @return void
         */
        public function delete(BaseRow $row)
        {
            return $this->database->deleteTable($this->getTableName($row), $row);
        }

        /**
         * Find all row table
         * @param $name
         * @return void 
         */
        public function findAll($name)
        {
            return $this->database->selectTable($name);
        }

        /**
         * Find row by id
         * @param $name
         * @return void
         */
        public function findById($name,$id)
        {
            return $this->database->findObjById($name,$id);
        }
    }  
