<?php
    include_once('C:\laragon\www\PHP_OOP\interface\IEnity.php');

    abstract class BaseRow implements IEnity
    {
        protected int $id;
        protected string $name;

        public function __contruct($id,$name)
        {
            $this->id = $id;
            $this->name = $name;
        }

        /**
         * Set Id by Row
         * @param $id
         */
        public function setId($id)
        {
            $this->id = $id;
        }

        /**
         * Get name by row
         * 
         * @return int
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set name
         * @param $name
         * @return void
         */
        public function setName($name)
        {
            $this->name = $name;
        }

        /**
         * Get name by row
         * 
         * @return string
         */
        public function getName()
        {
            return $this->name;
        }
    }
