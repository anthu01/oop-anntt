<?php
include_once 'C:\laragon\www\PHP_OOP\entity\Product.php';
include_once 'C:\laragon\www\PHP_OOP\entity\Category.php';
include_once 'C:\laragon\www\PHP_OOP\entity\Accessotion.php';


class Database 
{
    public $productTable = [];
    public $categoryTable = [];
    public $accessotionTable =[];
    protected static $instants = null;

    const  product = 'productTable';
    const  category = 'categoryTable';
    const  accessotion = 'accessotionTable';

    public static function getInstants()
    {
        if (self::$instants == null)
        {
            self::$instants = new Database();
        }
        return self::$instants;
    }

    /**
     * Insert Row to table
     * @param $name
     * @param $row
     * @return mixed
     */
    public function insertTable($name, BaseRow $row)
    {
        if ($name==self::product){
            return $this->productTable[] = $row;
        } elseif ($name==self::category){
            return $this->categoryTable[] = $row; 
        }elseif($name==self::accessotion){
            return $this->accessotionTable[] = $row;
        }
    }
    
    /**
     * Select array from table
     * @param $name
     * @return mixed
     */
    public function selectTable($name)
    {
        if($name==self::product){
            return $this->productTable;
        }elseif($name==self::category){
            return $this->categoryTable;
        }elseif($name ==self::accessotion){
            return $this->accessotionTable;
        }
    } 

    /**
     * Update Row to table
     * @param $name
     * @param $row
     * @return mixed
     */
    public function updateTable($name, BaseRow $row)
    { 
        if($name==self::product){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getId() == $row->getId()){
                    $this->{$name}[$key] = $row;
                }
            }
            return $this->productTable;
        }elseif($name==self::category){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getId() == $row->getId()){
                    $this->{$name}[$key] = $row;
                }
            }
            return $this->categoryTable;
        }elseif($name==self::accessotion){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getId() == $row->getId()){
                    $this->{$name}[$key] = $row;
                }
            }
            return $this->accessotionTable;
        }
    }

        /**
     * Delete Row  from able
     * @param $name
     * @param $row
     * @return void
     */
    public function deleteTable($name, BaseRow $row)
    {
        if($name==self::product){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getId() == $row->getId()){
                unset($this->{$name}[$key]);
                }
            }
            return $this->productTable;
        }elseif($name==self::category){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getId() == $row->getId()){
                unset($this->{$name}[$key]);
                }
            }
            return $this->categoryTable;
        }elseif($name==self::accessotion){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getId() == $row->getId()){
                unset($this->{$name}[$key]);
                }
            }
            return $this->accessotionTable;
        }
    }

    /**
     * Delete array from table
     * @param $name
     * @return void
     */
    public function truncateTable($name)
    {
        if($name==self::product){
            $this->productTable = array();
        }elseif($name==self::category){
            $this->categoryTable  = array();
        }elseif($name==self::accessotion){
            $this->accessotionTable = array();       
        }
    }

    /**
     * Update row table by id
     * @param $name
     * @param $id
     * @param $row
     * @return mixed
     */
    public function updateTableById($name,int $id, BaseRow $row)
    {
        if ($name==self::product){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getId() == $id){
                    $this->{$name}[$key] = $row;
                }  
            }
            return $this->productTable;      
        }elseif($name==self::category){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getId() == $id){
                    $this->{$name}[$key] = $row;
                }  
            }
            return $this->categoryTable;      
        }elseif($name==self::accessotion){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getId() == $id){
                    $this->{$name}[$key] = $row;
                }  
            }
            return $this->accessotionTable; 
        }
    }

    /**
     * Update row table by id
     * @param $name
     * @param $id
     * @return mixed
     */
    public function findObjById($name,$id)
    {
        if ($name==self::product){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getId() == $id){
                    return $this->{$name}[$key];
                }  
            }
      
        }elseif($name==self::category){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getId() == $id){
                    return $this->{$name}[$key];
                }     
            }
     
        }elseif($name==self::accessotion){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getId() == $id){
                    return $this->{$name}[$key];
                }  
            }
        }
    }

    /**
     * Update row table by name object
     * @param $name
     * @param $id
     * @return mixed
     */
    public function findObjByName($name,$nameObj){
        if ($name==self::product){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getName() == $nameObj){
                    return $this->{$name}[$key];
                }  
            }
        }elseif($name==self::category){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getName() == $nameObj){
                    return $this->{$name}[$key];
                }     
            }
        }elseif($name==self::accessotion){
            foreach($this->{$name} as $key=>$val){
                if($this->{$name}[$key]->getName() == $nameObj){
                    return $this->{$name}[$key];
                }  
            }
        }
    }
}
