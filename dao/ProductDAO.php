<?php 
    include_once('Database.php');
    include_once('C:\laragon\www\PHP_OOP\entity\Product.php');
    include_once('C:\laragon\www\PHP_OOP\asbtract\BaseDao.php');

    class ProductDAO extends BaseDao
    {
        // public $database;
        // function __construct() {
        //     $this->database = new Database();
        // }

        // public function insert(Product $row){
        //     return $this->database->insertTable('productTable', $row);
        // }

        // public function update(Product $row){
        //     return $this->database->updateTable('productTable', $row);
        // }

        // public function delete(Product $row){
        //     return $this->database->deleteTable('productTable', $row);
        // }

        // public function findAll(){
        //     return $this->database->productTable;
        // }

        // public function findById($id){
        //     return $this->database->findObjById('productTable',$id);
        // }

        /**
         * Find row by name object table
         * @param $name
         * @return void
         */
        public function findByName($nameProduct)
        {
            return $this->database->findObjByName('productTable',$nameProduct);
        }
    }
