<?php
    include_once('C:\laragon\www\PHP_OOP\entity\Category.php');
    include_once('C:\laragon\www\PHP_OOP\dao\CategoryDAO.php');
    
    class CategoryDaoDemo
    {
        public $database;

        function __construct()
        {
            $this->database = new CategoryDAO();
        }

        /**
        * Set table Category
        * @return array
        */
        function initCategoryDAO()
        {
            $category = new Category(1,'fgdfgdf');
            return $this->database->insert($category);
        }

        /**
        * Test insert row to table
        * @return array
        */
        public function insertTest()
        {
            $category = new Category(2,'sdfdsf');
            return $this->database->insert($category);
        }

        /**
        * Test update row to table
        * @return array
        */
        public function updateTest()
        {
            $category = new Category(1,'5dfgdf');
            return $this->database->update($category);
        }

        /**
        * Test delete row
        */
        public function deleteTest()
        {
            $category = new Category(1,'gffjdsf');
            return $this->database->delete($category);
        }

        /**
        * Test find all row table
        * @return array
        */
        public function findAllTest()
        {
            print_r($this->database->findAll());
        }

        /**
        * Test find row by id
        * @return array
        */
        public function findByIdTest()
        {
            print_r($this->database->findById(1));
        }
    }
