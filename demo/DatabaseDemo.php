<?php
    include_once 'C:\laragon\www\PHP_OOP\dao\Database.php';
    include_once 'C:\laragon\www\PHP_OOP\entity\Category.php';

    
    class DatabaseDemo
    {
        public $database;

        function __construct()
        {
            $this->database = Database::getInstants();
        }

        /**
        * Test insert row to table
        * @return array
        */
        public function insertTableTest()
        {
            $cate = new Category(1,'fdssfs');
            $product = new Product(1,'cxxx',2,120);
            $accessotion = new Accessotion(1,'jgf');
            print_r($this->database->insertTable('categoryTable',$cate));
            print_r($this->database->insertTable('productTable',$product));
            print_r($this->database->insertTable('accessotionTable',$accessotion));
        }

        /**
        * Test select table
        * @return array
        */
        public function selectTableTest()
        {
           return $this->database->selectTable('categoryTable');
        }

        /**
        * Test update row to table
        * @return array
        */
        public function updateTableTest()
        {
            $cate1 = new Category(1,'555555');
            print_r($this->database->updateTable('categoryTable',$cate1));
        }

        /**
        * Test delete row from table
        * @return array
        */
        public function deleteTableTest()
        {
            $cate1 = new Category(1,'555555');
            return $this->database->deleteTable('categoryTable',$cate1);
        }

        /**
        *Test delete array from table
        * @return array
        */
        public function truncateTableTest()
        {
            return $this->database->truncateTable('categoryTable');
        }

        /**
        *Set Table
        * @return array
        */
        public function initDatabase()
        {
            //Product
            $product = new Product(1,'cxxx',2,120);
            print_r($this->database->insertTable('productTable',$product));
            print_r($this->database->insertTable('productTable',$product));
            print_r($this->database->insertTable('productTable',$product));
            print_r($this->database->insertTable('productTable',$product));
            print_r($this->database->insertTable('productTable',$product));
            print_r($this->database->insertTable('productTable',$product));
            print_r($this->database->insertTable('productTable',$product));
            print_r($this->database->insertTable('productTable',$product));
            print_r($this->database->insertTable('productTable',$product));
            print_r($this->database->insertTable('productTable',$product));

            //Category
            $cate = new Category(1,'fdssfs');
            print_r($this->database->insertTable('categoryTable',$cate));
            print_r($this->database->insertTable('categoryTable',$cate));
            print_r($this->database->insertTable('categoryTable',$cate));
            print_r($this->database->insertTable('categoryTable',$cate));
            print_r($this->database->insertTable('categoryTable',$cate));
            print_r($this->database->insertTable('categoryTable',$cate));
            print_r($this->database->insertTable('categoryTable',$cate));
            print_r($this->database->insertTable('categoryTable',$cate));
            print_r($this->database->insertTable('categoryTable',$cate));
            print_r($this->database->insertTable('categoryTable',$cate));

            //Accessotion
            $accessotion = new Accessotion(1,'jgf');
            print_r($this->database->insertTable('accessotionTable',$accessotion));
            print_r($this->database->insertTable('accessotionTable',$accessotion));
            print_r($this->database->insertTable('accessotionTable',$accessotion));
            print_r($this->database->insertTable('accessotionTable',$accessotion));
            print_r($this->database->insertTable('accessotionTable',$accessotion));
            print_r($this->database->insertTable('accessotionTable',$accessotion));
            print_r($this->database->insertTable('accessotionTable',$accessotion));
            print_r($this->database->insertTable('accessotionTable',$accessotion));
            print_r($this->database->insertTable('accessotionTable',$accessotion));
            print_r($this->database->insertTable('accessotionTable',$accessotion));
        }

        /**
        * Test print all table 
        * @return array
        */
        public function printTableTest()
        {
            print_r($this->database);
        }

        /**
        * Test update row by id 
        * @return array
        */
        public function updateTableByIdTest()
        {
            $this->database->updateTableById('categoryTable',1,array('id'=>  1,'name'=> 'dfdsf'));
        }
    }

?>
