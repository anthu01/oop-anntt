<?php
    include_once('C:\laragon\www\PHP_OOP\entity\Product.php');
    include_once('C:\laragon\www\PHP_OOP\dao\ProductDao.php');

class ProductDaoDemo
{
    public $database;

    function __construct()
    {
        $this->database = new ProductDAO();
    }

    /**
     * Set Table  Product
     *@return array
     */
    function initProductDAO()
    {
        $product = new Product(1,'111',2,230);
        return $this->database->insert($product);
    }

    /**
     * Test insert row to table
     * @return array
     */
    function insertTest()
    {
        $product = new Product(1,'dfdsf',2,210);
        return $this->database->insert($product);
    }

    /**
     * Test update row to table
     * @return array
     */
    function updateTest()
    {
        $product = new Product(1,'222',2,220);
        return $this->database->update($product);
    }

    /**
     * Test delete row
     */
    function deleteTest()
    {
        $product = new Product(1,'2222',2,220);
        return $this->database->delete($product);
    }

    /**
     * Test find all row table
     * @return array
     */
    function findAllTest()
    {
        print_r($this->database->findAll());
    }

    /**
     * Test find row by id
     * @return array
     */
    function findByIdTest()
    {
        print_r($this->database->findById(1));
    }


    /**
     * Test find row by name
     * @return array
     */
    function findByNameTest()
    {
        print_r($this->database->findByName('111'));
    }
}
