<?php
require ('C:\laragon\www\PHP_OOP\entity\Product.php');

    class ProductDemo
    {
        public function createProduct($id, $name, $categoryid, $price)
        {
            $this->id =$id;
            $this->name = $name;
            $this->categoryid = $categoryid;
            $this->price= $price;
        }

        public function printProduct($product)
        {
            $print = "Id: {$this->id} <br>"; 
            $print .= "Name: {$this->name} <br>";
            $print .= "CategoryId: {$this->categoryid} <br>";
            $print .= "Price: {$this->price}  <br>";
            return $print;
        }
    }
