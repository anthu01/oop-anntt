<?php
    include_once('C:\laragon\www\PHP_OOP\asbtract\BaseRow.php');

    class Product extends BaseRow
    {
        public int $categoryid;
        public int $price;

        public function __construct($id, $name, $categoryid, $price)
        {
            $this->id = $id;
            $this->name = $name;
            $this->categoryid = $categoryid;
            $this->price = $price;
        }
    }
