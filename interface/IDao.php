<?php
    interface IDao
    {
        /**
         * Insert Row to table
         * @param $row
         * @return mixed
         */
        public function insert( BaseRow $row);

        /**
         * Update Row to table
         * @param $row
         * @return mixed 
         */
        public function update(BaseRow $row);
        
        /**
         * Delete Row from table
         * @param $row
         * @return void 
         */
        public function delete(BaseRow $row);

        /**
         * Find all row to table
         * @param $name
         * @return void 
         */
        public function findAll($name);

        /**
         * Find row by id
         * @param $name
         * @return void 
         */
        public function findById($name,$id);
    }
