<?php
    interface IEnity
    {
        /**
         * Set id
         * @param $id
         * @return void
         */
        public function setId($id);

        /**
         * Get name by row
         * 
         * return int
         */
        public function getId();

        /**
         * Set name
         * @param $name
         * @return void
         */
        public function setName($name);

        /**
         * Get name by row
         * 
         * return string
         */
        public function getName();
    }